//
//  main.m
//  fdghdgfh
//
//  Created by zhangruolei on 2018/3/9.
//  Copyright © 2018年 zhangruolei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
